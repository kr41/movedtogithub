Moved to GitHub
===============

Since Bitbucket shuts down Mercurial support,
there is no reason to stay here anymore.

So I've moved all my repos to [GitHub](https://github.com/kr41/).
